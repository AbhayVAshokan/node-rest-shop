const express = require('express')
const routes = express.Router();
const mongoose = require('mongoose');

const Orders = require('../models/order');

routes.get('/', (req, res, next) => {
    res.status(200).json({
        message: 'inside GET module of /orders',
    })
});

routes.post('/', (req, res, next) => {

    const orders = new Orders({
        _id: mongoose.Types.ObjectId(),
        product: req.body.productId,
        quantity: req.body.quantity,
    });

    orders.save()
        .then((result) => {
            console.log(result);
            res.status(201).json(result)
        })
        .catch((error) => {
            console.log(error.message);
            res.json(error);
        });
});

module.exports = routes;