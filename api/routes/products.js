const express = require('express');
const mongoose = require('mongoose');
const routes = express.Router();

const Product = require('../models/product');

routes.get('/', (req, res, next) => {
    Product.find()
        .select('name price _id')
        .exec()
        .then((result) => {
            const response = {
                count: result.length,
                product: result.map(doc => {
                    return {
                        _id: doc._id,
                        name: doc.name,
                        price: doc.price,
                        url: 'localhost:8000/products/' + doc._id,
                    };
                }),
            };

            console.log('here');

            res.status(200).json(response);
        })
        .catch((err) => {
            res.status(500).json(err);
        });
});

routes.get('/:productId', (req, res, next) => {
    Product.findById(req.params.productId)
        .exec()
        .then((result) => {
            if (result) {
                res.status(200).json(result);
                console.log(result);
            } else {
                res.status(404).json({
                    message: 'Id does not exist',
                });
            }

        })
        .catch((err) => {
            res.status(500).json(err);
            console.log(err);
        })
});

routes.post('/', (req, res, next) => {
    const product = new Product({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        price: req.body.price,
    });

    product.save()
        .then((result) => {
            console.log(result);
            res.json({
                product: product,
            });
        })
        .catch((err) => {
            console.log(err);
            res.status(500).json(err);
        });

});

routes.patch('/:productId', (req, res, next) => {
    const updateOps = {};
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value;
    }

    Product.update({ _id: req.params.productId }, { $set: updateOps })
        .exec()
        .then((result) => {
            res.status(200).json(result);
        })
        .catch((err) => {
            res.status(500).json(err);
        });
});

routes.delete('/:productId', (req, res, next) => {
    Product.findByIdAndRemove(req.params.productId)
        .exec()
        .then((result) => {
            if (result) {
                res.status(200).json(result);
            } else {
                res.status(404).json({
                    message: 'ID not found',
                });
            }
        })
        .catch((err) => {
            res.status(500).json(err);
        });
});

module.exports = routes;